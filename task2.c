#include "allocate.h"

/* if there are two cpus, load the Task2 */

void load_task2(process_t **all_process_array, int num_process)
{
    /* these for store the turnaround time and exe time for performance statistics*/
    int turn_around_array[num_process - 1];
    int ini_exe_array[num_process - 1];
    int performance_index = 0;

    process_t *cpu_running_lst_0[num_process];
    process_t *cpu_running_lst_1[num_process];

    /* for printing finish processes */
    int finished_array_star_index = 0;
    int finished_array_index = 0;
    int finished_array[num_process];

    int running_lst_len_0 = 0, running_lst_len_1 = 0;
    int timer = 0;
    int finished_process = 0;
    int rest_proc = 0;

    /* let cpus processing the process until all processes finished */
    while (finished_process != num_process)
    {
        process_t *new_processes[num_process];

        /* allocate all new arrived processes in this second */
        allocate_all_processes_T2(new_processes, all_process_array, num_process, timer,
                                  &rest_proc, cpu_running_lst_0, cpu_running_lst_1,
                                  &running_lst_len_0, &running_lst_len_1);

        /* record the index for finished processes */
        finished_array_star_index = finished_array_index;

        /* proessing each cpu list */
        processing_lst(&finished_array_index, finished_array, cpu_running_lst_0, &running_lst_len_0, cpu_running_lst_1, running_lst_len_1, CPU_0,
                       &finished_process, &rest_proc, timer, turn_around_array, ini_exe_array, &performance_index);
        processing_lst(&finished_array_index, finished_array, cpu_running_lst_1, &running_lst_len_1, cpu_running_lst_0, running_lst_len_0, CPU_1,
                       &finished_process, &rest_proc, timer, turn_around_array, ini_exe_array, &performance_index);

        /* print the finished processes */
        for (int k = finished_array_star_index; k < finished_array_index; k++)
        {
            printf("%d,FINISHED,pid=%d,proc_remaining=%d\n", timer + 1, finished_array[k], rest_proc);
        }

        timer++;
    }

    /* Performance statistics: */
    show_stat(timer, turn_around_array, ini_exe_array, num_process);
}

/* allocated all new processes to the cpus*/

void allocate_all_processes_T2(process_t **new_processes, process_t **all_process_array,
                               int num_process, int timer, int *rest_proc, process_t **cpu_running_lst_0,
                               process_t **cpu_running_lst_1, int *running_lst_len_0, int *running_lst_len_1)
{
    int new_proc_num;
    /* checking new arrive process add to the new_processes aray, and get the number of it*/
    new_proc_num = adding_new_process(new_processes, all_process_array, num_process, timer, rest_proc);

    /* allocate these new processes to the cpu by requirement*/
    for (int i = 0; i < new_proc_num; i++)
    {
        proc_allocation(cpu_running_lst_0, cpu_running_lst_1,
                        running_lst_len_0, running_lst_len_1, new_processes[i]);
    }
}

/* allocated the processes to the cpu */

void proc_allocation(process_t **cpu_running_lst_0, process_t **cpu_running_lst_1,
                     int *running_lst_len_0, int *running_lst_len_1, process_t *new_processes)
{
    int remain_time_0;
    int remain_time_1;

    /* if it is a parallelisable process, two subprocess create and add to each cpu*/
    if (new_processes->is_parallelisable == PARALL_SYMBOL)
    {
        int total_cpus = 2;
        int sub_exe_time = (int)ceil(((double)(new_processes->execution_time) / total_cpus) + EXTRA_TIME);
        process_t *sub_process_0 = (process_t *)malloc(sizeof(process_t));
        process_t *sub_process_1 = (process_t *)malloc(sizeof(process_t));
        assert(sub_process_0 != NULL);
        assert(sub_process_1 != NULL);
        creat_sub_process(sub_process_0, new_processes, CPU_0, sub_exe_time);
        creat_sub_process(sub_process_1, new_processes, CPU_1, sub_exe_time);

        cpu_running_lst_0[*running_lst_len_0] = sub_process_0;
        *running_lst_len_0 += 1;
        cpu_running_lst_1[*running_lst_len_1] = sub_process_1;
        *running_lst_len_1 += 1;
    }
    else
    {
        /* if it is non-parallelisable, compare the remaining time for each cpu and add the process*/
        remain_time_0 = get_remaining_time(cpu_running_lst_0, running_lst_len_0);
        remain_time_1 = get_remaining_time(cpu_running_lst_1, running_lst_len_1);
        if (remain_time_0 <= remain_time_1)
        {
            cpu_running_lst_0[*running_lst_len_0] = new_processes;
            *running_lst_len_0 = *running_lst_len_0 + 1;
        }
        else
        {
            cpu_running_lst_1[*running_lst_len_1] = new_processes;
            *running_lst_len_1 = *running_lst_len_1 + 1;
        }
    }
}

/* create a sub process */

void creat_sub_process(process_t *sub_process, process_t *new_processes, int cpu_id, int sub_exe_time)
{
    sub_process->arrive_time = new_processes->arrive_time;
    sub_process->pro_id = new_processes->pro_id;
    sub_process->sub_id = cpu_id;
    sub_process->execution_time = sub_exe_time;
    sub_process->ini_exe_time = new_processes->execution_time;
    sub_process->is_parallelisable = new_processes->is_parallelisable;
    sub_process->status = NOT_START;
}

/* get the remaining time form each cpu list*/

int get_remaining_time(process_t **cpu_running_lst, int *running_lst_len)
{
    int remain_time = 0;
    for (int i = 0; i < *running_lst_len; i++)
    {
        remain_time += cpu_running_lst[i]->execution_time;
    }
    return remain_time;
}

/* proceesing the cpu list in one second*/

void processing_lst(int *finished_array_index, int *finished_array, process_t **cpu_running_lst,
                    int *running_lst_len, process_t **another_lst, int another_running_lst_len,
                    int cpu_id, int *finished_process, int *rest_proc, int timer,
                    int *turn_around_array, int *ini_exe_array, int *performance_index)
{
    /* if no process, terminate */
    if (*running_lst_len == 0)
    {
        return;
    }

    process_t *cur_processing_pro = cpu_running_lst[HEAD_PROC];

    sort_lst(cpu_running_lst, *running_lst_len);

    /* if the cur_processing_pro is not the shortest job after sorted, change it 
     * status, and make the first one in list become the cur_processing pro*/
    if (cur_processing_pro != cpu_running_lst[HEAD_PROC])
    {
        cur_processing_pro->status = NOT_START;
        cur_processing_pro = cpu_running_lst[HEAD_PROC];
    }

    /* check the running pro, and print it */
    check_running_pro_T2(cur_processing_pro, cpu_running_lst, timer, cpu_id);

    cur_processing_pro->execution_time -= 1;

    /* if current process finished */
    if (cur_processing_pro->execution_time == 0)
    {
        cur_processing_pro->status = FINISHED;
        delete_head(cpu_running_lst, running_lst_len);

        int sub_pro_still_exists = find_sub_process(cur_processing_pro->pro_id, another_lst, another_running_lst_len);
        char cur_pro_paralleisable = cur_processing_pro->is_parallelisable;

        if (cur_pro_paralleisable == NON_PARALL_SYMBOL || (cur_pro_paralleisable == PARALL_SYMBOL && sub_pro_still_exists == FALSE))
        {

            /* if it will be finished in this second, put it to the finished array */
            finished_array[*finished_array_index] = cur_processing_pro->pro_id;
            *finished_array_index += 1;

            /* proc remaning -1 */
            --*rest_proc;

            /* record information for the performance statistics */
            record_perform_info(turn_around_array, ini_exe_array, performance_index, cur_processing_pro, timer+1);

            *finished_process += 1;
        }

        free(cur_processing_pro);
    }
}


/* check the running pro and print it for task2 */
void check_running_pro_T2(process_t *cur_processing_pro, process_t **cpu_running_lst,
                          int timer, int cpu_id)
{

    if (cur_processing_pro->status == NOT_START)
    {

        cur_processing_pro->status = RUNNING;

        /* if it is parallelisable print with sub process*/

        if (cur_processing_pro->is_parallelisable == 'p')
        {
            printf("%d,RUNNING,pid=%d.%d,remaining_time=%d,cpu=%d\n",
                   timer, cur_processing_pro->pro_id, cur_processing_pro->sub_id, cur_processing_pro->execution_time, cpu_id);
        }
        else
        {
            printf("%d,RUNNING,pid=%d,remaining_time=%d,cpu=%d\n",
                   timer, cur_processing_pro->pro_id, cur_processing_pro->execution_time, cpu_id);
        }
    }
}