#include "allocate.h"

/* if there is only one cpu, load the Task1 */

void load_task1(process_t **all_process_array, int num_process)
{

    /* these for store the turnaround time and exe time for performance statistics*/
    int turn_around_array[num_process - 1];
    int ini_exe_array[num_process - 1];
    int performance_index = 0;

    process_t *cpu_running_lst[num_process];
    int rest_proc = num_process;
    int running_lst_len = 0;
    int timer = 0;

    /* check the is there any process will be arrived at 0, if exists ,add them to the running lst */
    check_arrive_and_add(num_process, all_process_array, cpu_running_lst, &running_lst_len, &timer);

    /* sort the list, the first one in the list will be the shortest-time-remaining process*/
    sort_lst(cpu_running_lst, running_lst_len);

    process_t *cur_processing_pro;
    int finished_process = 0;

    /* let the cpu processing the process until all processes finished */
    while (finished_process != num_process)
    {
        /* first one is the shortest-time-remaning process*/
        cur_processing_pro = cpu_running_lst[HEAD_PROC];

        /* check the running pro, and print it */
        check_running_pro_T1(cur_processing_pro, cpu_running_lst, timer);

        cur_processing_pro->execution_time -= 1;
        timer++;

        /* check the finish pro, and print it */
        check_finished_pro_T1(cur_processing_pro, &rest_proc, timer, cpu_running_lst,
                              &finished_process, &running_lst_len, turn_around_array,
                              ini_exe_array, &performance_index);

        check_arrive_and_add(num_process, all_process_array, cpu_running_lst, &running_lst_len, &timer);

        sort_lst(cpu_running_lst, running_lst_len);

        if (cur_processing_pro != NULL && cpu_running_lst[HEAD_PROC] != cur_processing_pro)
        {
            cur_processing_pro->status = NOT_START;
        }
    }

    /* Performance statistics: */
    show_stat(timer, turn_around_array, ini_exe_array, num_process);
}

/* check the running pro in task1 */

void check_running_pro_T1(process_t *cur_processing_pro, process_t **cpu_running_lst, int timer)
{

    if (cur_processing_pro->status == NOT_START)
    {
        printf("%d,RUNNING,pid=%d,remaining_time=%d,cpu=0\n", timer,
               cur_processing_pro->pro_id, cur_processing_pro->execution_time);
        cur_processing_pro->status = RUNNING;
    }
}

/* check the finished pro in task1 */

void check_finished_pro_T1(process_t *cur_processing_pro, int *rest_proc, int timer,
                           process_t **cpu_running_lst, int *finished_process, int *running_lst_len,
                           int *turn_around_array, int *ini_exe_array, int *performance_index)
{
    if (cur_processing_pro->execution_time == 0)
    {
        cur_processing_pro->status = FINISHED;
        /* if the the shortest process finished, deleted it, processing the next one*/
        delete_head(cpu_running_lst, running_lst_len);
        *finished_process += 1;
        printf("%d,FINISHED,pid=%d,proc_remaining=%d\n", timer, cur_processing_pro->pro_id, --*rest_proc);

        /* record information for the performance statistics*/
        record_perform_info(turn_around_array, ini_exe_array, performance_index, cur_processing_pro, timer);
        free(cur_processing_pro);
    }
}

/* check the new arrive processes, and add them to the running list*/

void check_arrive_and_add(int num_process, process_t **all_process_array,
                          process_t **cpu_running_lst, int *running_lst_len, int *timer)
{
    for (int i = 0; i < num_process; i++)
    {
        if (all_process_array[i]->arrive_time == *timer)
        {
            cpu_running_lst[*running_lst_len] = all_process_array[i];
            *running_lst_len += 1;
        }
    }
}
