/* allocate.h*/

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define NOT_START 0            /* define the not start */
#define RUNNING 1              /* define the running */
#define FINISHED 2             /* define the finish */
#define SINGLE_PROCESSOR "1"   /* define the single processor */
#define DOUBLE_PROCESSORS "2"  /* define the double processors */
#define HEAD_PROC 0            /* define the first proc in the list*/
#define PROCESS_SIZE 3000      /* define the max process size for loading*/
#define BUFFER_SIZE 100        /* define the buffer size for read one line of file*/
#define TRUE 1                 /* define the true value*/
#define FALSE 0                /* define the false value*/
#define ONE_HUNDRED 100        /* define the 100 */
#define EXTRA_TIME 1           /* defiene the extra time for synchronisation */
#define FILE_SYMBOL "-f"       /* character "-f" */
#define PROCESSORS_SYMBOL "-p" /* character "-p" */
#define PARALL_SYMBOL 'p'      /* char for prallel */
#define NON_PARALL_SYMBOL 'n'  /* char for non-prallel */
#define CPU_0 0                /* define for Task2:  cpu_id: 0*/
#define CPU_1 1                /* define for Task2:  cpu_id: 1*/
#define EMPTY 0                /* define for EMPTY 1*/

typedef struct process process_t;
struct process
{
    int arrive_time;
    int pro_id;
    int sub_id;
    int execution_time;
    int ini_exe_time;       /* for record the initial execution time*/
    int status;
    char is_parallelisable;
};

typedef struct list_processes list_processes_t;
struct list_processes
{
    process_t **cpu_running_lst;
    int arrive_time_record;
    int running_lst_len;
    int cpu_id;
    int lst_remain_time;
};

/* helping functions (tools) */

int get_process_num(char *filename);
void decide_task_to_load(char *processors, process_t **all_process_array, int num_process);
void load_process(char *filename, process_t **all_process_array);
void delete_head(process_t **cpu_running_lst, int *running_lst_len);
void sort_lst(process_t **cpu_running_lst, int running_lst_len);
void record_perform_info(int *turn_around_array, int *ini_exe_array, int *performance_index,
                         process_t *cur_processing_pro, int timer);
void show_stat(int timer, int *turn_around_array, int *ini_exe_array, int num_process);
int find_sub_process(int parallel_pro_id, process_t **cpu_queue, int queue_len);
int adding_new_process(process_t **new_processes, process_t **all_process_array,
                       int num_process, int timer, int *rest_proc);
void free_all_process_array(process_t** all_process_array);                      

/* task1 */

void load_task1(process_t **all_process_array, int num_process);
void check_arrive_and_add(int num_process, process_t **all_process_array,
                          process_t **cpu_running_lst, int *running_lst_len, int *timer);
void check_running_pro_T1(process_t *cur_processing_pro, process_t **cpu_running_lst, int timer);
void check_finished_pro_T1(process_t *cur_processing_pro, int *rest_proc, int timer,
                           process_t **cpu_running_lst, int *finished_process, int *running_lst_len,
                           int *turn_around_array, int *ini_exe_array, int *performance_index);

/* task2 */

void load_task2(process_t **all_process_array, int num_process);
int get_remaining_time(process_t **cpu_running_lst, int *running_lst_len);
void proc_allocation(process_t **cpu_running_lst_0, process_t **cpu_running_lst_1,
                     int *running_lst_len_0, int *running_lst_len_1, process_t *new_process);
void creat_sub_process(process_t *sub_process, process_t *new_processes, int cpu_id, int sub_exe_time);
void processing_lst(int *finished_array_index, int *finished_array, process_t **cpu_running_lst,
                    int *running_lst_len, process_t **another_lst, int another_running_lst_len,
                    int cpu_id, int *finished_process, int *rest_proc, int timer,
                    int *turn_around_array, int *ini_exe_array, int *performance_index);
void allocate_all_processes_T2(process_t **new_processes, process_t **all_process_array,
                               int num_process, int timer, int *rest_proc, process_t **cpu_running_lst_0,
                               process_t **cpu_running_lst_1, int *running_lst_len_0, int *running_lst_len_1);
void check_running_pro_T2(process_t *cur_processing_pro, process_t **cpu_running_lst,
                          int timer, int cpu_id);

/* task3 */

void load_task3(process_t **all_process_array, int num_process, char *processors);
void allocate_por_to_cpus(int num_processors, list_processes_t **all_cpus_lst, process_t *new_process);
void insert_to_running_lst(list_processes_t *queue, process_t *new_process);
void sort_all_cpus_lst(list_processes_t **all_cpus_lst, int num_processors);
int get_sub_num(int parallel_exe_time, int num_processors);
void check_finished_pro_T3(int *finished_array, int *finished_array_index,
                           process_t *cur_processing_pro, int *rest_proc);
void check_running_pro_T3(process_t *cur_processing_pro, process_t **cpu_running_lst,
                          int timer, list_processes_t *selected_cpu_lst);
void processing_each_cpu_lst(int *finished_process, int *rest_proc, int timer, list_processes_t **all_cpus_lst,
                             int cpu_id, int num_processors, int *turn_around_array, int *ini_exe_array,
                             int *performance_index, int *finished_array_index, int *finished_array);
void allocate_all_processes_T3(process_t **new_processes, process_t **all_process_array,
                               int num_process, int timer, int *rest_proc, int num_processors,
                               list_processes_t **all_cpus_lst);