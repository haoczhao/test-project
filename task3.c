#include "allocate.h"

/* if there are more than two cpus, load the Task3 */

void load_task3(process_t **all_process_array, int num_process, char *processors)
{
    /* create mulitple cups list based on the given processors number*/
    int num_processors = atoi(processors);
    list_processes_t **all_cpus_lst;
    all_cpus_lst = malloc(sizeof(list_processes_t *) * num_processors);

    /* these for store the turnaround time and exe time for performance statistics*/
    int turn_around_array[num_process - 1];
    int ini_exe_array[num_process - 1];
    int performance_index = 0;

    /* allocate memory for the all cpus list*/
    for (int i = 0; i < num_processors; i++)
    {
        all_cpus_lst[i] = malloc(sizeof(list_processes_t));
        assert(all_cpus_lst[i]);
        all_cpus_lst[i]->cpu_running_lst = malloc(sizeof(process_t *) * num_process);
        assert(all_cpus_lst[i]->cpu_running_lst);
        all_cpus_lst[i]->arrive_time_record = 0;
        all_cpus_lst[i]->running_lst_len = 0;
        all_cpus_lst[i]->lst_remain_time = 0;
        all_cpus_lst[i]->cpu_id = i;
    }

    /* for running the cpus*/
    process_t *new_processes[num_process];
    int timer = 0;
    int rest_proc = 0;
    int finished_process = 0;

    /* for printing finish processes */
    int finished_array_star_index = 0;
    int finished_array_index = 0;
    int finished_array[num_process];

    /* ruuning the whole processes */
    while (finished_process != num_process)
    {
        /* get all new processes and allocating them to the respond cpus*/

        allocate_all_processes_T3(new_processes, all_process_array, num_process, timer,
                                  &rest_proc, num_processors, all_cpus_lst);

        /* sort the cpu list*/
        sort_all_cpus_lst(all_cpus_lst, num_processors);

        /* update index for later finish printing*/
        finished_array_star_index = finished_array_index;

        /* processing all cpus, for 1 second*/
        for (int j = 0; j < num_processors; j++)
        {
            int cpu_id = j;
            processing_each_cpu_lst(&finished_process, &rest_proc, timer, all_cpus_lst, cpu_id, num_processors,
                                    turn_around_array, ini_exe_array, &performance_index, &finished_array_index, finished_array);
        }

        /* print the finished processes */
        for (int k = finished_array_star_index; k < finished_array_index; k++)
        {
            printf("%d,FINISHED,pid=%d,proc_remaining=%d\n", timer + 1, finished_array[k], rest_proc);
        }

        timer++;
    }

    /* Performance statistics: */
    show_stat(timer, turn_around_array, ini_exe_array, num_process);

    /* free the all_cpu_lst */
    for(int i=0; i<num_processors; i++){
        for(int j=0;j<all_cpus_lst[i]->running_lst_len;j++){
            free(all_cpus_lst[i]->cpu_running_lst[j]);
        }
        free(all_cpus_lst[i]->cpu_running_lst);
        free(all_cpus_lst[i]);
    }
    free(all_cpus_lst);
}

/* allocate all new arrive processes to cpus */

void allocate_all_processes_T3(process_t **new_processes, process_t **all_process_array,
                               int num_process, int timer, int *rest_proc, int num_processors,
                               list_processes_t **all_cpus_lst)
{
    /* get the new arrived process, and record the number of new*/
    int new_proc_num = adding_new_process(new_processes, all_process_array, num_process, timer, rest_proc);

    /* allocate these processes to the cpus*/
    for (int i = 0; i < new_proc_num; i++)
    {
        allocate_por_to_cpus(num_processors, all_cpus_lst, new_processes[i]);
    }
}

/* allocated one new process to cpu*/
void allocate_por_to_cpus(int num_processors, list_processes_t **all_cpus_lst, process_t *new_process)
{
    /* if it is a paralle process, creating the sub process and assign to all cpus*/
    if (new_process->is_parallelisable == PARALL_SYMBOL)
    {

        int i, num_sub_processes;
        int parallel_exe_time = new_process->execution_time;
        /* find the sub process number need to be created*/
        num_sub_processes = get_sub_num(parallel_exe_time, num_processors);
        sort_all_cpus_lst(all_cpus_lst, num_processors);

        int sub_exe_time = (int)ceil(((double)(new_process->execution_time) / num_sub_processes) + EXTRA_TIME);

        for (i = 0; i < num_sub_processes; i++)
        {
            process_t *sub_process = (process_t *)malloc(sizeof(process_t));
            sub_process->arrive_time = new_process->arrive_time;
            sub_process->pro_id = new_process->pro_id;
            sub_process->sub_id = i;
            sub_process->execution_time = sub_exe_time;
            sub_process->ini_exe_time = new_process->ini_exe_time;
            sub_process->status = NOT_START;
            sub_process->is_parallelisable = new_process->is_parallelisable;
            insert_to_running_lst(all_cpus_lst[i], sub_process);
        }
    }
    else
    {
        /* otherwise assign the non-parallel to the least busy cpu*/
        sort_all_cpus_lst(all_cpus_lst, num_processors);
        insert_to_running_lst(all_cpus_lst[HEAD_PROC], new_process);
    }
}

/* using bubble sort to sort the cpu list, first based on the arrive time, then remain time, then cpu id*/

void sort_all_cpus_lst(list_processes_t **all_cpus_lst, int num_processors)
{
    int i, j;
    list_processes_t *temp;
    for (i = 0; i < num_processors - 1; i++)
    {
        for (j = 0; j < num_processors - 1 - i; j++)
        {
            if ((all_cpus_lst[j]->arrive_time_record) > (all_cpus_lst[j + 1]->arrive_time_record))
            {
                temp = all_cpus_lst[j];
                all_cpus_lst[j] = all_cpus_lst[j + 1];
                all_cpus_lst[j + 1] = temp;
            }
            else if ((all_cpus_lst[j]->lst_remain_time) > (all_cpus_lst[j + 1]->lst_remain_time))
            {
                temp = all_cpus_lst[j];
                all_cpus_lst[j] = all_cpus_lst[j + 1];
                all_cpus_lst[j + 1] = temp;
            }
            /* if the execution_time is same, compare the pro id*/
            else if ((all_cpus_lst[j]->lst_remain_time) == (all_cpus_lst[j + 1]->lst_remain_time))
            {
                if (all_cpus_lst[j]->cpu_id > all_cpus_lst[j + 1]->cpu_id)
                {
                    temp = all_cpus_lst[j];
                    all_cpus_lst[j] = all_cpus_lst[j + 1];
                    all_cpus_lst[j + 1] = temp;
                }
            }
        }
    }
}

/* take the smallest one as the number of subprocess for creating*/

int get_sub_num(int parallel_exe_time, int num_processors)
{
    if (parallel_exe_time <= num_processors)
    {
        return parallel_exe_time;
    }
    else
    {
        return num_processors;
    }
}

/* insert new process to the specified cpu list, also addng reamin and arrive time*/

void insert_to_running_lst(list_processes_t *selected_cpu_lst, process_t *new_process)
{
    int new_process_remain_time = new_process->execution_time;
    int new_process_arrive_time = new_process->arrive_time;
    selected_cpu_lst->cpu_running_lst[selected_cpu_lst->running_lst_len] = new_process;
    selected_cpu_lst->running_lst_len += 1;
    selected_cpu_lst->lst_remain_time += new_process_remain_time;
    selected_cpu_lst->arrive_time_record += new_process_arrive_time;
}

/* processing the specified cpu list for one second run*/

void processing_each_cpu_lst(int *finished_process, int *rest_proc, int timer, list_processes_t **all_cpus_lst,
                             int cpu_id, int num_processors, int *turn_around_array, int *ini_exe_array,
                             int *performance_index, int *finished_array_index, int *finished_array)
{
    /* if there is no process for run, exit */
    if (all_cpus_lst[cpu_id]->running_lst_len == EMPTY)
    {
        return;
    }

    /* find the selected cpu lst, and extract the cpu running list*/
    list_processes_t *selected_cpu_lst = all_cpus_lst[cpu_id];
    process_t **cur_cpu_running_lst = selected_cpu_lst->cpu_running_lst;
    process_t *cur_processing_pro = cur_cpu_running_lst[HEAD_PROC];

    /* sort the list, so the first process is the shortest job */
    sort_lst(cur_cpu_running_lst, selected_cpu_lst->running_lst_len);

    /* if the cur processing is not the first process, 
       change it status, and reassign cur processing pro */
    if (cur_processing_pro != cur_cpu_running_lst[HEAD_PROC])
    {
        cur_processing_pro->status = NOT_START;
        cur_processing_pro = cur_cpu_running_lst[HEAD_PROC];
    }

    // check the running process, and print it //
    check_running_pro_T3(cur_processing_pro, cur_cpu_running_lst, timer, selected_cpu_lst);

    cur_processing_pro->execution_time -= 1;
    selected_cpu_lst->lst_remain_time -= 1;

    /* if current process finished */
    if (cur_processing_pro->execution_time == 0)
    {
        cur_processing_pro->status = FINISHED;

        if (cur_processing_pro->is_parallelisable == PARALL_SYMBOL)
        {
            delete_head(cur_cpu_running_lst, &selected_cpu_lst->running_lst_len);
            int cur_par_id = cur_processing_pro->pro_id;
            /* if the sub process is still exist in other cpu running list, it still not finished*/
            for (int i = 0; i < num_processors; i++)
            {
                int sub_pro_still_exists = find_sub_process(cur_par_id, all_cpus_lst[i]->cpu_running_lst, all_cpus_lst[i]->running_lst_len);
                if ((i != cur_par_id) && (sub_pro_still_exists == TRUE))
                {
                    return;
                }
            }

            /*  if all sub processes finished, adding to the finished array */
            check_finished_pro_T3(finished_array,finished_array_index,cur_processing_pro,rest_proc);

            /* record information for the performance statistics */
            record_perform_info(turn_around_array, ini_exe_array, performance_index, cur_processing_pro, timer+1);

            *finished_process += 1;
        }
        else
        {
            /*  if finished, adding to the finished array */
            check_finished_pro_T3(finished_array,finished_array_index,cur_processing_pro,rest_proc);

            /* record information for the performance statistics */
           record_perform_info(turn_around_array, ini_exe_array, performance_index, cur_processing_pro, timer+1);

            delete_head(cur_cpu_running_lst, &selected_cpu_lst->running_lst_len);
            *finished_process += 1;
        }
        free(cur_processing_pro);
    }
}

/* check the running process, and print it for Task3 */

void check_running_pro_T3(process_t *cur_processing_pro, process_t **cpu_running_lst,
                          int timer, list_processes_t *selected_cpu_lst)
{
    /* run the cur processing pro and print it information depend on the 'n' or 'p'*/
    if (cur_processing_pro->status != RUNNING)
    {
        cur_processing_pro->status = RUNNING;
        if (cur_processing_pro->is_parallelisable == PARALL_SYMBOL)
        {
            printf("%d,RUNNING,pid=%d.%d,remaining_time=%d,cpu=%d\n",
                   timer, cur_processing_pro->pro_id, cur_processing_pro->sub_id,
                   cur_processing_pro->execution_time, selected_cpu_lst->cpu_id);
        }
        else
        {
            printf("%d,RUNNING,pid=%d,remaining_time=%d,cpu=%d\n",
                   timer, cur_processing_pro->pro_id, cur_processing_pro->execution_time,
                   selected_cpu_lst->cpu_id);
        }
    }
}


/* check the finished processes, adding to finish array */

void check_finished_pro_T3(int *finished_array, int *finished_array_index, process_t *cur_processing_pro, int *rest_proc)
{
    finished_array[*finished_array_index] = cur_processing_pro->pro_id;
    *finished_array_index += 1;
    --*rest_proc;
}