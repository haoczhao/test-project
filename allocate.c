#include "allocate.h"

/* main program of the function */

int main(int argc, char *argv[])
{

    char *filename;
    char *processors;
    int num_process;
    process_t **all_process_array = (process_t **)malloc(sizeof(process_t *) * PROCESS_SIZE);
    assert(all_process_array != NULL);

    /* read and store the filename and processors from the commend */
    for (int j = 0; j < argc; j++)
    {
        if (strcmp(argv[j], FILE_SYMBOL) == 0)
        {
            filename = argv[j + 1];
        }
        if (strcmp(argv[j], PROCESSORS_SYMBOL) == 0)
        {
            processors = argv[j + 1];
        }
    }

    /* get the number of process form the txt file */
    num_process = get_process_num(filename);

    /* load the txt file process into the all_process_array for store purpose */
    load_process(filename, all_process_array);

    /* choose the task to load, depend on the number of processors */
    decide_task_to_load(processors, all_process_array, num_process);


    return 0;
}
