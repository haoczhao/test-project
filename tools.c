#include "allocate.h"

/* get the processes number by using fopen */

int get_process_num(char *filename)
{

    FILE *fp;
    char txt[BUFFER_SIZE];
    int num_line = 0;

    if ((fp = fopen(filename, "r")) == NULL)
    {
        printf("ERROR");
    }

    while (fgets(txt, sizeof(txt), fp) != NULL)
    {
        num_line++;
    }

    fclose(fp);
    return num_line;
}

/* load the whole porcesses to the array for store */

void load_process(char *filename, process_t **all_process_array)
{
    FILE *fp = NULL;
    char txt[BUFFER_SIZE];
    int index = 0;

    if ((fp = fopen(filename, "r")) == NULL)
    {
        printf("ERROR");
    }

    while (fgets(txt, sizeof(txt), fp) != NULL)
    {
        char tmp_parallelisable;
        int tmp_execution_time;
        int tmp_arrive_time;
        int tmp_pro_id;
        /* using sscanf to assign each value */
        sscanf(txt, "%d %d %d %c\n", &tmp_arrive_time, &tmp_pro_id, &tmp_execution_time, &tmp_parallelisable);
        /* create a new pro element with assigned value, and add it to the array*/
        process_t *new_pro = (process_t *)malloc(sizeof(process_t));
        assert(new_pro != NULL);
        new_pro->arrive_time = tmp_arrive_time;
        new_pro->pro_id = tmp_pro_id;
        new_pro->execution_time = tmp_execution_time;
        new_pro->ini_exe_time = tmp_execution_time;
        new_pro->is_parallelisable = tmp_parallelisable;
        new_pro->status = NOT_START;
        all_process_array[index] = new_pro;
        index++;
    }
    fclose(fp);
}

/* by deducing the number of processors to decide load which task */

void decide_task_to_load(char *processors, process_t **all_process_array, int num_process)
{
    if (strcmp(processors, SINGLE_PROCESSOR) == 0)
    {
        load_task1(all_process_array, num_process);
    }
    else if (strcmp(processors, DOUBLE_PROCESSORS) == 0)
    {
        load_task2(all_process_array, num_process);
    }
    else
    {
        load_task3(all_process_array, num_process, processors);
    }
}

/* show the performance statistics */

void show_stat(int timer, int *turn_around_array, int *ini_exe_array, int num_process)
{

    /* show the average turn around time on the termianl*/

    int avg_turn_around;
    double turn_around_sum = 0;
    for (int i = 0; i < num_process; i++)
    {
        turn_around_sum += (double)turn_around_array[i];
    }
    avg_turn_around = (int)ceil(turn_around_sum / num_process);
    printf("Turnaround time %d\n", avg_turn_around);

    /* show the average and max overhead time on the array*/

    double overhead_array[num_process - 1];
    double max = 0;
    double overhead_sum = 0;
    double ave_overhead;
    for (int j = 0; j < num_process; j++)
    {
        overhead_array[j] = (double)turn_around_array[j] / ini_exe_array[j];
        if (overhead_array[j] > max)
        {
            max = overhead_array[j];
        }
    }

    for (int k = 0; k < num_process; k++)
    {
        overhead_sum += overhead_array[k];
    }
    ave_overhead = overhead_sum / num_process;
    /*using this method to round the decimal places*/
    max = round(max * ONE_HUNDRED) / ONE_HUNDRED;
    ave_overhead = round(ave_overhead * ONE_HUNDRED) / ONE_HUNDRED;
    printf("Time overhead %g %g\n", max, ave_overhead);

    /* show the Makespan on the array*/
    printf("Makespan %d\n", timer);
}

/* delete the first process in the running list */

void delete_head(process_t **cpu_running_lst, int *running_lst_len)
{
    for (int i = 1; i < *running_lst_len; i++)
    {
        cpu_running_lst[i - 1] = cpu_running_lst[i];
    }
    *running_lst_len = *running_lst_len - 1;
}

/* using the buuble sort to sort the whole running list, when new process arrive */

void sort_lst(process_t **cpu_running_lst, int running_lst_len)
{
    int i, j;
    process_t *temp;
    for (i = 0; i < running_lst_len - 1; i++)
    {
        for (j = 0; j < running_lst_len - 1 - i; j++)
        {
            if ((cpu_running_lst[j]->execution_time) > (cpu_running_lst[j + 1]->execution_time))
            {
                temp = cpu_running_lst[j];
                cpu_running_lst[j] = cpu_running_lst[j + 1];
                cpu_running_lst[j + 1] = temp;
            }
            /* if the execution_time is same, compare the pro id*/
            else if ((cpu_running_lst[j]->execution_time) == (cpu_running_lst[j + 1]->execution_time))
            {
                if (cpu_running_lst[j]->pro_id > cpu_running_lst[j + 1]->pro_id)
                {
                    temp = cpu_running_lst[j];
                    cpu_running_lst[j] = cpu_running_lst[j + 1];
                    cpu_running_lst[j + 1] = temp;
                }
            }
        }
    }
}

/* find the sub process whether exists on another cpu running list*/

int find_sub_process(int parallel_pro_id, process_t **cpu_running_lst, int running_lst_len)
{
    int founded = FALSE;
    if (running_lst_len != 0)
    {
        for (int i = 0; i < running_lst_len; i++)
        {
            if (cpu_running_lst[i]->pro_id == parallel_pro_id)
            {
                founded = TRUE;
                return founded;
            }
        }
    }
    return founded;
}

/* adding new process to the temp list, and return the number of arrivals*/

int adding_new_process(process_t **new_processes, process_t **all_process_array,
                       int num_process, int timer, int *rest_proc)
{
    int new_proc_num = 0;
    for (int i = 0; i < num_process; i++)
    {
        if ((all_process_array[i]->arrive_time) == timer)
        {
            new_processes[new_proc_num] = all_process_array[i];
            new_proc_num++;
            *rest_proc += 1;
        }
    }

    /* sort the new arrive list*/
    sort_lst(new_processes, new_proc_num);

    return new_proc_num;
}

/* when process finished, record the performance information in arrays */

void record_perform_info(int *turn_around_array, int *ini_exe_array, int *performance_index,
                         process_t *cur_processing_pro, int timer)
{
    turn_around_array[*performance_index] = timer - cur_processing_pro->arrive_time;
    ini_exe_array[*performance_index] = cur_processing_pro->ini_exe_time;
    *performance_index += 1;
}

/* free the array*/

void free_all_process_array(process_t** all_process_array){
    for(int i=0 ; i<PROCESS_SIZE; i++){
        free(all_process_array[i]);
    }
    free(all_process_array);
}


