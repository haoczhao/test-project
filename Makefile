allocate: allocate.c allocate.h tools.c task1.c task2.c task3.c
	gcc -Wall allocate.c allocate.h tools.c task1.c task2.c task3.c -o allocate -lm

.PHONY : clean

clean :
	rm -f $(allocate)